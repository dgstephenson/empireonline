Games = new Mongo.Collection("games");

Games.helpers({
  getFactions: function() { return Factions.find({gameId: this._id}); },
  getMyFaction: function() { return Factions.findOne({gameId: this._id, userId: Meteor.userId()}); },
  countFactions: function() { return Factions.find({gameId: this._id}).count(); },
  checkWaiting: function() { return this.status==="waiting"; },
  start: function() { if(this.checkReady()) { Meteor.call("startGame",this._id); } },
  checkReady: function() { 
    ready = this.getMyFaction() && this.countFactions()>1 && (this.status==="waiting");
    if(ready) { this.getFactions().forEach(function(faction) {
      ready = ready && faction.ready;
    })}
    return ready;
  },
})

Meteor.methods({
  createGame: function(name) {
    check(name,String);
    check(Meteor.userId(),String);
    var user = Meteor.users.findOne(Meteor.userId());
    if(user) {
      if(name && name!=="") {
        if(Games.findOne({name: name})) {
          throw new Meteor.Error("invalid-game-name","This game name is already in use.");
        } else {
          Games.insert({
            name: name, 
            status: "waiting", 
            round: 0, 
            phase: 0,
          });
        }
      } else throw new Meteor.Error("empty-game-name","The game name must be a non-empty string.");
    } else throw new Meteor.Error("invalid-user-id","You must be logged in before you can create a game.");
  },
  startGame: function(gameId) {
    check(gameId,String);
    check(Meteor.userId(),String);
    game = Games.findOne(gameId);
    if(game.checkReady()) {
      if(Factions.findOne({userId: Meteor.userId(), gameId: gameId})) {
        Games.update(gameId, {$set:{status:"active"}} );
      } else throw new Meteor.Error("invalid-user-id","You must join this game before you can start it.");
    } else throw new Meteor.Error("game-not-ready","This game is not ready to start.");
  }
});