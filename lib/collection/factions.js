Factions = new Mongo.Collection("factions");

Meteor.methods({
  createFaction: function(gameId) {
    check(gameId,String);
    var userId = Meteor.userId();
    try { 
      check(userId,String);
    } catch(error) {
      throw new Meteor.Error("invalid-user-id","You must be logged in before you can join a game."); 
    };
    if(userId) {
      duplicateFactions = Factions.find({userId: userId, gameId: gameId}).count();
      if(duplicateFactions===0) {
        Factions.insert({
          userId: userId,
          gameId: gameId,
          support: 0,
          credits: 0,
          population: 0,
          colonies: 0,
          goods: 0,
          loans: 0,
          ready: false,
        });
      } else throw new Meteor.Error("duplicate-faction","You are already in this game.");
    } else throw new Meteor.Error("invalid-user-id","You must be logged in before you can join a game.");
  },
  setReady: function(gameId,ready) {
    check(gameId,String);
    check(ready,Boolean);
    var userId = Meteor.userId();
    check(userId,String);
    Factions.update({userId: userId, gameId: gameId}, {$set:{ready:ready}} );
  },
});

Factions.helpers({
  getUsername: function() {
    return Meteor.users.findOne(this.userId).username;
  },
  setReady: function(ready) {
    Meteor.call("setReady",this.gameId,ready);
  },
})