Router.configure({
  layoutTemplate: "layout",
  loading: "loading",
});

Router.route("/",{name: "home"});

Router.route("/game/:gameName",{
  name: "game", 
  data: function(){return {game: Games.findOne({name: this.params.gameName})}}, 
});