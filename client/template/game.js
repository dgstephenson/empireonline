Template.game.onCreated(function() { 
  var instance = this;
  instance.subscribe("games");
  instance.subscribe("factions");
});

Template.game.events({
  "click .btn-join" : function(event,template) {
    event.preventDefault();
    Meteor.call("createFaction",template.data.game._id);
  },
  "click .btn-start" : function(event,template) {
    event.preventDefault();
    template.data.game.start();
  }
});

Template.game.helpers({
   
}); 