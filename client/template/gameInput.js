Template.gameInput.onCreated(function() { 
  var instance = this;
  instance.subscribe("games");
  instance.subscribe("factions");
});

Template.gameInput.events({
  "click .btn-ready" : function(event,template) {
    event.preventDefault();
    faction = Factions.findOne({userId: Meteor.userId(), gameId: template.data.game._id});
    faction.setReady(true);
  },
  "click .btn-unready" : function(event,template) {
    event.preventDefault();
    faction = Factions.findOne({userId: Meteor.userId(), gameId: template.data.game._id});
    faction.setReady(false);
  }
});

Template.gameInput.helpers({
   
}); 