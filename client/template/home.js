Template.home.onCreated(function() { 
  var instance = this;
  instance.subscribe("games");
  instance.subscribe("factions");
});

Template.home.helpers({
  getGames: function(){return Games.find();},
});

Template.home.events({
  "submit .create-game-form" : function(event,template) {
    event.preventDefault();
    var gameName = $(event.target).find('[name=game-name]').val();
    Meteor.call("createGame",gameName);
  }
});